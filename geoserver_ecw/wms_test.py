from owslib.wms import WebMapService
import argparse

parser = argparse.ArgumentParser(description='WMS test')
parser.add_argument('--url', type=str, help='WMS URL', default='http://localhost:8080/geoserver/wms&version=1.3.0&')
args = parser.parse_args()

wms = WebMapService(args.url, version='1.3.0')
print(wms.contents)