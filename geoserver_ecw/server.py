from flask import Flask, Response, make_response, request as flask_request
import gzip
import requests
from requests.api import request

app = Flask(__name__)

@app.route("/geoserver/geonode/ows")
def wms():
    print(flask_request.base_url)
    print(flask_request.host_url)
    print(flask_request.host)
    print(flask_request.root_url)
    return http_response()
    #return file_response()

def http_response():
    r = requests.get("http://localhost:8080/geoserver/ows?service=WMS&version=1.3.0&request=GetCapabilities", headers={'Cache-Control': 'max-age=0'})
    #print(r.text)
    return gzip_response(r.text)

def file_response(gzip=True):
   with open('geoserver_wms_example.xml') as file:
        xml = file.read() 
        if gzip:
            return gzip_response(xml)
        else:
            return string_reponse(xml)   

def string_reponse(string):
    return Response(string, mimetype='text/xml')

def gzip_response(string):    
    content = gzip.compress(string.encode('utf8'), 5)
    response = make_response(content)
    response.headers['Content-length'] = len(content)
    response.headers['Content-Type'] = 'text/xml'
    response.headers['Content-Encoding'] = 'gzip'
    return response

if __name__ == "__main__":
   app.run()