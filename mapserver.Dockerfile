FROM debian:buster as builder
LABEL maintainer="PDOK dev <https://github.com/PDOK/mapserver-docker/issues>"

ENV DEBIAN_FRONTEND noninteractive
ENV TZ Europe/Amsterdam

RUN apt-get -y update && \
    apt-get install -y --no-install-recommends \
        ca-certificates \
        gettext \
        xz-utils \
        cmake \
        gcc \
        g++ \
        libfreetype6-dev \
        libglib2.0-dev \
        libcairo2-dev \
        git \        
        locales \
        make \
        patch \
        openssh-server \
        protobuf-compiler \
        protobuf-c-compiler \
        software-properties-common \
        wget && \
    rm -rf /var/lib/apt/lists/*

# Setup build env for PROJ
RUN apt-get clean \
	&& apt-get update -y \
	&& apt-get upgrade -y \
    && apt-get install -y --fix-missing --no-install-recommends \
            build-essential \ 
            cmake unzip libtool automake \
            zlib1g-dev libsqlite3-dev pkg-config sqlite3

RUN update-locale LANG=C.UTF-8

ENV HARFBUZZ_VERSION 2.8.2

RUN cd /tmp && \
        wget https://github.com/harfbuzz/harfbuzz/releases/download/$HARFBUZZ_VERSION/harfbuzz-$HARFBUZZ_VERSION.tar.xz && \
        tar xJf harfbuzz-$HARFBUZZ_VERSION.tar.xz && \
        cd harfbuzz-$HARFBUZZ_VERSION && \
        ./configure && \
        make && \
        make install && \
        ldconfig

RUN apt-get -y update && \
    apt-get install -y --no-install-recommends \
        libcurl4-gnutls-dev \
        libfribidi-dev \
        libgif-dev \
        libjpeg-dev \
        libpq-dev \
        librsvg2-dev \      
        libpng-dev \
        libfreetype6-dev \
        libjpeg-dev \
        libexempi-dev \
        libfcgi-dev \
        #libgdal-dev \
        libgeos-dev \
        #libproj-dev \
        librsvg2-dev \
        libprotobuf-dev \
        libprotobuf-c-dev \
        libprotobuf-c1 \
        libxslt1-dev && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get -y update --fix-missing



# # Setup build env for GDAL
RUN apt-get clean \
	&& apt-get update -y \
	&& apt-get upgrade -y \
    && apt-get install -y --fix-missing --no-install-recommends \
       libcharls-dev \
       libopenjp2-7-dev libcairo2-dev \
       python3-dev python3-numpy \
       libgif-dev liblzma-dev libgeos-dev \
       curl libcurl4-gnutls-dev libxml2-dev libexpat-dev libxerces-c-dev \
       libnetcdf-dev libpoppler-dev libpoppler-private-dev \
       libspatialite-dev \
       swig libhdf4-alt-dev libhdf5-serial-dev \
       libfreexl-dev unixodbc-dev libwebp-dev libepsilon-dev \
       liblcms2-2 libpcre3-dev libcrypto++-dev libdap-dev libfyba-dev \
       libkml-dev default-libmysqlclient-dev \
       libogdi3.2-dev \
       libcfitsio-dev libzstd-dev \
       #openjdk-11-jdk \
       libpq-dev libssl-dev libboost-dev libtiff-dev \
       autoconf automake bash-completion libarmadillo-dev 

# RUN git clone https://github.com/team-charls/charls.git \
#     && cd charls \
#     && git fetch \	
#     && git checkout 1.x-master \
#     && mkdir -p build \
#     && cd build \
#     && cmake .. \
#     && make -j$(nproc) \
#     && make install
 
ARG PROJ_DATUMGRID_LATEST_LAST_MODIFIED
RUN \
     mkdir -p /build_projgrids/${PROJ_INSTALL_PREFIX}/share/proj \
     && curl -LOs http://download.osgeo.org/proj/proj-datumgrid-latest.zip \
     && unzip -q -j -u -o proj-datumgrid-latest.zip  -d /build_projgrids/${PROJ_INSTALL_PREFIX}/share/proj \
     && rm -f *.zip

# Order layers starting with less frequently varying ones
#RUN \
#    mkdir -p ${PROJ_INSTALL_PREFIX}/share/proj \
#    && curl -LOs http://download.osgeo.org/proj/proj-datumgrid-latest.zip \
#    && unzip -j -u -o proj-datumgrid-latest.zip  -d ${PROJ_INSTALL_PREFIX}/share/proj \
#    && curl -LOs http://download.osgeo.org/proj/proj-datumgrid-europe-latest.zip \
#    && unzip -j -u -o proj-datumgrid-europe-latest.zip -d ${PROJ_INSTALL_PREFIX}/share/proj \
#    && curl -LOs http://download.osgeo.org/proj/proj-datumgrid-oceania-latest.zip \
#    && unzip -j -u -o proj-datumgrid-oceania-latest.zip -d ${PROJ_INSTALL_PREFIX}/share/proj \
#    && curl -LOs http://download.osgeo.org/proj/proj-datumgrid-world-latest.zip  \
#    && unzip -j -u -o proj-datumgrid-world-latest.zip -d ${PROJ_INSTALL_PREFIX}/share/proj \
#    && curl -LOs http://download.osgeo.org/proj/proj-datumgrid-north-america-latest.zip \
#    && unzip -j -u -o proj-datumgrid-north-america-latest.zip -d ${PROJ_INSTALL_PREFIX}/share/proj  \
#    && rm -f *.zip

# Build likbkea
#ARG KEA_VERSION=c6d36f3db5e4
#RUN wget -q https://bitbucket.org/chchrsc/kealib/get/${KEA_VERSION}.zip \
#    && unzip -q ${KEA_VERSION}.zip \
#    && rm -f ${KEA_VERSION}.zip \
#    && cd chchrsc-kealib-${KEA_VERSION}/trunk \
#    && cmake . -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=Release \
#        -DCMAKE_INSTALL_PREFIX=/usr -DHDF5_INCLUDE_DIR=/usr/include/hdf5/serial \
#        -DHDF5_LIB_PATH=/usr/lib/x86_64-linux-gnu/hdf5/serial -DLIBKEA_WITH_GDAL=OFF \
#    && make -j$(nproc) \
#    && make install DESTDIR="/build_thirdparty" \
#    && make install \
#    && cd ../.. \
#    && rm -rf chchrsc-kealib-${KEA_VERSION} \
#    && for i in /build_thirdparty/usr/lib/*; do strip -s $i 2>/dev/null || /bin/true; done \
#    && for i in /build_thirdparty/usr/bin/*; do strip -s $i 2>/dev/null || /bin/true; done

# Build mongo-c-driver
ARG MONGO_C_DRIVER_VERSION=1.13.0
RUN mkdir mongo-c-driver \
    && wget -q https://github.com/mongodb/mongo-c-driver/releases/download/${MONGO_C_DRIVER_VERSION}/mongo-c-driver-${MONGO_C_DRIVER_VERSION}.tar.gz -O - \
        | tar xz -C mongo-c-driver --strip-components=1 \
    && cd mongo-c-driver \
    && mkdir build_cmake \
    && cd build_cmake \
    && cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DENABLE_TESTS=NO -DCMAKE_BUILD_TYPE=Release \
    && make -j$(nproc) \
    && make install DESTDIR="/build_thirdparty" \
    && make install \
    && cd ../.. \
    && rm -rf mongo-c-driver \
    && rm /build_thirdparty/usr/lib/x86_64-linux-gnu/*.a \
    && for i in /build_thirdparty/usr/lib/x86_64-linux-gnu/*; do strip -s $i 2>/dev/null || /bin/true; done \
    && for i in /build_thirdparty/usr/bin/*; do strip -s $i 2>/dev/null || /bin/true; done

# Build mongocxx
ARG MONGOCXX_VERSION=3.4.0
RUN mkdir mongocxx \
    && wget -q https://github.com/mongodb/mongo-cxx-driver/archive/r${MONGOCXX_VERSION}.tar.gz -O - \
        | tar xz -C mongocxx --strip-components=1 \
    && cd mongocxx \
    && mkdir build_cmake \
    && cd build_cmake \
    && cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DBSONCXX_POLY_USE_BOOST=ON -DMONGOCXX_ENABLE_SLOW_TESTS=NO -DCMAKE_BUILD_TYPE=Release \
    && make -j$(nproc) \
    && make install DESTDIR="/build_thirdparty" \
    && make install \
    && cd ../.. \
    && rm -rf mongocxx \
    && for i in /build_thirdparty/usr/lib/x86_64-linux-gnu/*; do strip -s $i 2>/dev/null || /bin/true; done \
    && for i in /build_thirdparty/usr/bin/*; do strip -s $i 2>/dev/null || /bin/true; done

# Build tiledb
ARG TILEDB_VERSION=2.0.0
RUN mkdir tiledb \
    && wget -q https://github.com/TileDB-Inc/TileDB/archive/${TILEDB_VERSION}.tar.gz -O - \
        | tar xz -C tiledb --strip-components=1 \
    && cd tiledb \
    && mkdir build_cmake \
    && cd build_cmake \
    && ../bootstrap --prefix=/usr \
    && make -j$(nproc) \
    && make install-tiledb DESTDIR="/build_thirdparty" \
    && make install-tiledb \
    && cd ../.. \
    && rm -rf tiledb \
    && for i in /build_thirdparty/usr/lib/x86_64-linux-gnu/*; do strip -s $i 2>/dev/null || /bin/true; done \
    && for i in /build_thirdparty/usr/bin/*; do strip -s $i 2>/dev/null || /bin/true; done

# Build openjpeg
#ARG OPENJPEG_VERSION=2.3.1
#RUN if test "${OPENJPEG_VERSION}" != ""; then ( \
#    wget -q https://github.com/uclouvain/openjpeg/archive/v${OPENJPEG_VERSION}.tar.gz \
#    && tar xzf v${OPENJPEG_VERSION}.tar.gz \
#    && rm -f v${OPENJPEG_VERSION}.tar.gz \
#    && cd openjpeg-${OPENJPEG_VERSION} \
#    && cmake . -DBUILD_SHARED_LIBS=ON  -DBUILD_STATIC_LIBS=OFF -DCMAKE_BUILD_TYPE=Release \
#        -DCMAKE_INSTALL_PREFIX=/usr \
#    && make -j$(nproc) \
#    && make install \
#    && mkdir -p /build_thirdparty/usr/lib/x86_64-linux-gnu \
#    && rm -f /usr/lib/x86_64-linux-gnu/libopenjp2.so* \
#    && mv /usr/lib/libopenjp2.so* /usr/lib/x86_64-linux-gnu \
#    && cp -P /usr/lib/x86_64-linux-gnu/libopenjp2.so* /build_thirdparty/usr/lib/x86_64-linux-gnu \
#    && for i in /build_thirdparty/usr/lib/x86_64-linux-gnu/*; do strip -s $i 2>/dev/null || /bin/true; done \
#    && cd .. \
#    && rm -rf openjpeg-${OPENJPEG_VERSION} \
#    ); fi

RUN apt-get update -y \
    && apt-get install -y --fix-missing --no-install-recommends rsync ccache
ARG RSYNC_REMOTE

# Build PROJ
# Build PROJ
# ARG PROJ_INSTALL_PREFIX=/usr/local
# ARG PROJ_VERSION=master
# #ARG PROJ_VERSION=7.2
# RUN mkdir proj \
#     && wget -q https://github.com/OSGeo/proj.4/archive/${PROJ_VERSION}.tar.gz -O - \
#         | tar xz -C proj --strip-components=1 \
#     && cd proj \
#     && ./autogen.sh \
#     && if test "${RSYNC_REMOTE}" != ""; then \
#         echo "Downloading cache..."; \
#         rsync -ra ${RSYNC_REMOTE}/proj/ $HOME/; \
#         echo "Finished"; \
#         export CC="ccache gcc"; \
#         export CXX="ccache g++"; \
#         export PROJ_DB_CACHE_DIR="$HOME/.ccache"; \
#         ccache -M 100M; \
#     fi \
#     && CFLAGS='-DPROJ_RENAME_SYMBOLS -O2' CXXFLAGS='-DPROJ_RENAME_SYMBOLS -O2' \
#         ./configure --prefix=${PROJ_INSTALL_PREFIX} --disable-static \
#     && make -j$(nproc) \
#     && make install DESTDIR="/build" \
#     && make install \
#     && if test "${RSYNC_REMOTE}" != ""; then \
#         ccache -s; \
#         echo "Uploading cache..."; \
#         rsync -ra --delete $HOME/.ccache ${RSYNC_REMOTE}/proj/; \
#         echo "Finished"; \
#         rm -rf $HOME/.ccache; \
#         unset CC; \
#         unset CXX; \
#     fi \
#     && cd .. \
#     && rm -rf proj \
#     && PROJ_SO=$(readlink /build${PROJ_INSTALL_PREFIX}/lib/libproj.so | sed "s/libproj\.so\.//") \
#     && PROJ_SO_FIRST=$(echo $PROJ_SO | awk 'BEGIN {FS="."} {print $1}') \
#     && mv /build${PROJ_INSTALL_PREFIX}/lib/libproj.so.${PROJ_SO} /build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so.${PROJ_SO} \
#     && ln -s libinternalproj.so.${PROJ_SO} /build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so.${PROJ_SO_FIRST} \
#     && ln -s libinternalproj.so.${PROJ_SO} /build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so \
#     && rm /build${PROJ_INSTALL_PREFIX}/lib/libproj.*  \
#     && ln -s libinternalproj.so.${PROJ_SO} /build${PROJ_INSTALL_PREFIX}/lib/libproj.so.${PROJ_SO_FIRST} \
#     && strip -s /build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so.${PROJ_SO} \
#     && for i in /build${PROJ_INSTALL_PREFIX}/bin/*; do strip -s $i 2>/dev/null || /bin/true; done
   
# Copy ERDAS ECW
RUN mkdir ./erdas/
COPY /erdas/ERDAS* erdas/
RUN mkdir /usr/local/hexagon \
    && cp -r erdas/Desktop_Read-Only/* /usr/local/hexagon \
    && rm -r /usr/local/hexagon/lib/x64 \
    && mv /usr/local/hexagon/lib/newabi/x64 /usr/local/hexagon/lib/x64 \
    && cp /usr/local/hexagon/lib/x64/release/libNCSEcw* /usr/local/lib \
    && ldconfig /usr/local/hexagon \
    && ldconfig /usr/local/lib

#ARG GDAL_VERSION=release/3.0
#ARG GDAL_VERSION=release/2.2.4
ARG GDAL_VERSION=2.4.4
ARG GDAL_RELEASE_DATE
ARG GDAL_BUILD_IS_RELEASE

# Build GDAL
# Build GDAL
RUN if test "${GDAL_VERSION}" = "master"; then \
        export GDAL_VERSION=$(curl -Ls https://api.github.com/repos/OSGeo/gdal/commits/HEAD -H "Accept: application/vnd.github.VERSION.sha"); \
        export GDAL_RELEASE_DATE=$(date "+%Y%m%d"); \
    fi \
    && if test "x${GDAL_BUILD_IS_RELEASE}" = "x"; then \
        export GDAL_SHA1SUM=${GDAL_VERSION}; \
    fi \
    && mkdir gdal \
    #&& wget -q https://github.com/OSGeo/gdal/archive/${GDAL_VERSION}.tar.gz -O - \
    && wget -q https://download.osgeo.org/gdal/${GDAL_VERSION}/gdal-${GDAL_VERSION}.tar.gz -O - \
        | tar xz -C gdal --strip-components=1 \
    && cd gdal \ 
    #&& cd gdal-${GDAL_VERSION} \
    && if test "${RSYNC_REMOTE}" != ""; then \
        echo "Downloading cache..."; \
        rsync -ra ${RSYNC_REMOTE}/gdal/ $HOME/; \
        echo "Finished"; \
        # Little trick to avoid issues with Python bindings
        printf "#!/bin/sh\nccache gcc \$*" > ccache_gcc.sh; \
        chmod +x ccache_gcc.sh; \
        printf "#!/bin/sh\nccache g++ \$*" > ccache_g++.sh; \
        chmod +x ccache_g++.sh; \
        export CC=$PWD/ccache_gcc.sh; \
        export CXX=$PWD/ccache_g++.sh; \
        ccache -M 1G; \
    fi \
    && ./configure --prefix=/usr --without-libtool \
    --with-hide-internal-symbols \
    #--with-jpeg12 \
    --with-python \ 
    #--with-spatialite --with-mysql --with-liblzma \
    #--with-webp --with-epsilon \
    #--with-proj=/build/usr/local \ 
    #--with-poppler \
    #--with-hdf5 --with-dods-root=/usr --with-sosi \
    #--with-libtiff=internal --with-rename-internal-libtiff-symbols \
    #--with-geotiff=internal --with-rename-internal-libgeotiff-symbols \
    #--with-kea=/usr/bin/kea-config
    #--with-mongocxxv3 --with-tiledb \
    --with-ecw=/usr/local/hexagon \
    --with-crypto \
    #--with-java=/usr/lib/jvm/java-11-openjdk-amd64 \
    #--with-java=${JAVA_HOME} \
    && make -j$(nproc) \ 
   	&& make install \
    && make install DESTDIR="/build" \   
    && cd .. \
    && rm -rf gdal \
    && mkdir -p /build_gdal_python/usr/lib \
    && mkdir -p /build_gdal_python/usr/bin \
    && mkdir -p /build_gdal_version_changing/usr/include \
    && mv /build/usr/lib/python3            /build_gdal_python/usr/lib \
    && mv /build/usr/lib                    /build_gdal_version_changing/usr \
    && mv /build/usr/include/gdal_version.h /build_gdal_version_changing/usr/include \
    && mv /build/usr/bin/*.py               /build_gdal_python/usr/bin \
    && mv /build/usr/bin                    /build_gdal_version_changing/usr \
    && for i in /build_gdal_version_changing/usr/lib/*; do strip -s $i 2>/dev/null || /bin/true; done \
    && for i in /build_gdal_python/usr/lib/python3/dist-packages/osgeo/*.so; do strip -s $i 2>/dev/null || /bin/true; done \
    && for i in /build_gdal_version_changing/usr/bin/*; do strip -s $i 2>/dev/null || /bin/true; done   
    
  	#&& cd ../../.. \

RUN date


RUN git clone --single-branch -b pdok-7-6-4-patch-3 https://github.com/pdok/mapserver/ /usr/local/src/mapserver

#RUN git clone https://github.com/MapServer/MapServer /usr/local/src/mapserver 

RUN mkdir /usr/local/src/mapserver/build && \
    cd /usr/local/src/mapserver/build && \
    cmake ../ \
        -DWITH_PROJ=ON \
        -DWITH_KML=OFF \
        -DWITH_SOS=OFF \
        -DWITH_WMS=ON \
        -DWITH_FRIBIDI=ON \
        -DWITH_HARFBUZZ=ON \
        -DWITH_ICONV=ON \
        -DWITH_CAIRO=ON \
        -DWITH_SVGCAIRO=OFF \
        -DWITH_RSVG=ON \
        -DWITH_MYSQL=OFF \
        -DWITH_FCGI=ON \
        -DWITH_GEOS=ON \
        -DWITH_POSTGIS=ON \
        -DWITH_GDAL=ON \
        -DWITH_OGR=ON \
        -DWITH_CURL=ON \
        -DWITH_CLIENT_WMS=ON \
        -DWITH_CLIENT_WFS=ON \
        -DWITH_WFS=ON \
        -DWITH_WCS=ON \
        -DWITH_LIBXML2=ON \
        -DWITH_THREAD_SAFETY=OFF \
        -DWITH_GIF=ON \
        -DWITH_PYTHON=OFF \
        -DWITH_PHP=OFF \
        -DWITH_PERL=OFF \
        -DWITH_RUBY=OFF \
        -DWITH_JAVA=OFF \
        -DWITH_CSHARP=OFF \
        -DWITH_ORACLESPATIAL=OFF \
        -DWITH_ORACLE_PLUGIN=OFF \
        -DWITH_MSSQL2008=OFF \
        -DWITH_SDE_PLUGIN=OFF \
        -DWITH_SDE=OFF \
        -DWITH_EXEMPI=ON \
        -DWITH_XMLMAPFILE=ON \
        -DWITH_V8=OFF \
        -DBUILD_STATIC=OFF \
        -DLINK_STATIC_LIBMAPSERVER=OFF \
        -DWITH_APACHE_MODULE=OFF \          
        -DWITH_POINT_Z_M=ON \
        -DWITH_GENERIC_NINT=OFF \
        -DWITH_PROTOBUFC=ON \
        -DCMAKE_PREFIX_PATH=/usr/lib && \
        make && \
        make install && \
        ldconfig

FROM pdok/lighttpd:1.4.53-buster as service
LABEL maintainer="PDOK dev <https://github.com/PDOK/mapserver-docker/issues>"

ENV DEBIAN_FRONTEND noninteractive
ENV TZ Europe/Amsterdam

COPY --from=builder /usr/local/bin /usr/local/bin
COPY --from=builder /usr/local/lib /usr/local/lib
COPY --from=builder  /build/usr/share/gdal/ /usr/share/gdal/
COPY --from=builder  /build/usr/include/ /usr/include/
COPY --from=builder  /build_gdal_python/usr/ /usr/
COPY --from=builder  /build_gdal_version_changing/usr/ /usr/

RUN ln -s /usr/local/lib/libgdal.so.20.4.4 /usr/local/lib/libgdal.so && ldconfig

RUN apt-get -y update && \
    apt-get install -y --no-install-recommends \
        ca-certificates \
        libpng16-16 \
        python-cairocffi-doc \
        libfreetype6 \
        libjpeg62-turbo \
        libfcgi0ldbl \
        libfribidi0 \
        libcurl4-gnutls-dev \
        libgeos-c1v5 \
        libglib2.0-0 \
        libproj13 \
        libxml2 \
        libxslt1.1 \
        libexempi8 \
        libpq5 \
        libfreetype6 \
        librsvg2-2 \
        libprotobuf17 \
        libprotobuf-c1 \
        gettext-base \
        libgif-dev \
        libjpeg-dev \
        libpq-dev \
        librsvg2-dev \      
        libpng-dev \
        libjpeg-dev \
        libexempi-dev \
        libfcgi-dev \
        #libgdal-dev \
        libgeos-dev \
        #libproj-dev \
        librsvg2-dev \
        libprotobuf-dev \
        libprotobuf-c-dev \
        libprotobuf-c1 \
        libxslt1-dev \
        wget \
        gnupg \
        libcharls-dev \
        libopenjp2-7-dev libcairo2-dev \
        python3-dev python3-numpy \
        libgif-dev liblzma-dev libgeos-dev \
        curl libcurl4-gnutls-dev libxml2-dev libexpat-dev libxerces-c-dev \
        libnetcdf-dev libpoppler-dev libpoppler-private-dev \
        libspatialite-dev \
        swig libhdf4-alt-dev libhdf5-serial-dev \
        libfreexl-dev unixodbc-dev libwebp-dev libepsilon-dev \
        liblcms2-2 libpcre3-dev libcrypto++-dev libdap-dev libfyba-dev \
        libkml-dev default-libmysqlclient-dev \
        libogdi3.2-dev \
        libcfitsio-dev libzstd-dev \
        #openjdk-11-jdk \
        libpq-dev libssl-dev libboost-dev libtiff-dev \
        autoconf automake bash-completion libarmadillo-dev \
    && rm -rf /var/lib/apt/lists/*

COPY etc/lighttpd.conf /lighttpd.conf
COPY etc/filter-map.lua /filter-map.lua

RUN chmod o+x /usr/local/bin/mapserv
RUN apt-get clean

ENV DEBUG 0
ENV MIN_PROCS 1
ENV MAX_PROCS 3
ENV MAX_LOAD_PER_PROC 4
ENV IDLE_TIMEOUT 20

EXPOSE 80

CMD ["lighttpd", "-D", "-f", "/lighttpd.conf"]
