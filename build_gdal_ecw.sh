#!/bin/bash
set -e

apt-get update -y 
apt-get install -y --fix-missing --no-install-recommends \
            software-properties-common build-essential ca-certificates \
            git make cmake wget unzip libtool automake \
            zlib1g-dev libsqlite3-dev pkg-config sqlite3 \
            libcharls-dev libopenjp2-7-dev libcairo2-dev \
            python3-dev python3-numpy \
            libpng-dev libjpeg-dev libgif-dev liblzma-dev libgeos-dev \
            curl libcurl4-gnutls-dev libxml2-dev libexpat1-dev libxerces-c-dev \
            libnetcdf-dev libpoppler-dev libpoppler-private-dev \
            libspatialite-dev swig libhdf4-alt-dev libhdf5-dev \
            libfreexl-dev unixodbc-dev libwebp-dev libepsilon-dev \
            liblcms2-2 libpcre3-dev libcrypto++-dev libdap-dev libfyba-dev \
            libkml-dev libmysqlclient-dev libogdi-dev \
            libcfitsio-dev openjdk-8-jdk libzstd-dev \
            libpq-dev libssl-dev libboost-dev libtiff-dev \
            autoconf automake bash-completion libarmadillo-dev


echo "------------Building Mongo-----------"
echo "-------------------------------------"
# Build mongo-c-driver
export MONGO_C_DRIVER_VERSION=1.13.0
mkdir -p mongo-c-driver \
    && wget -q https://github.com/mongodb/mongo-c-driver/releases/download/${MONGO_C_DRIVER_VERSION}/mongo-c-driver-${MONGO_C_DRIVER_VERSION}.tar.gz -O - \
        | tar xz -C mongo-c-driver --strip-components=1 \
    && cd mongo-c-driver \
    && mkdir build_cmake \
    && cd build_cmake \
    && cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DENABLE_TESTS=NO -DCMAKE_BUILD_TYPE=Release \
    && make -j$(nproc) \
    && make install \
    && cd ../.. \
    && rm -rf mongo-c-driver 
    # && rm /build_thirdparty/usr/lib/x86_64-linux-gnu/*.a \
    # && for i in /build_thirdparty/usr/lib/x86_64-linux-gnu/*; do strip -s $i 2>/dev/null || /bin/true; done \
    # && for i in /build_thirdparty/usr/bin/*; do strip -s $i 2>/dev/null || /bin/true; done

# Build mongocxx
export MONGOCXX_VERSION=3.4.0
mkdir -p mongocxx \
    && wget -q https://github.com/mongodb/mongo-cxx-driver/archive/r${MONGOCXX_VERSION}.tar.gz -O - \
        | tar xz -C mongocxx --strip-components=1 \
    && cd mongocxx \
    && mkdir build_cmake \
    && cd build_cmake \
    && cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DBSONCXX_POLY_USE_BOOST=ON -DMONGOCXX_ENABLE_SLOW_TESTS=NO -DCMAKE_BUILD_TYPE=Release \
    && make -j$(nproc) \
    && make install \
    && cd ../.. \
    && rm -rf mongocxx 
    # && for i in /build_thirdparty/usr/lib/x86_64-linux-gnu/*; do strip -s $i 2>/dev/null || /bin/true; done \
    # && for i in /build_thirdparty/usr/bin/*; do strip -s $i 2>/dev/null || /bin/true; done

echo "------------Building TileDB----------"
echo "-------------------------------------"
# Build tiledb
export TILEDB_VERSION=2.0.0
mkdir -p tiledb \
    && wget -q https://github.com/TileDB-Inc/TileDB/archive/${TILEDB_VERSION}.tar.gz -O - \
        | tar xz -C tiledb --strip-components=1 \
    && cd tiledb \
    && mkdir build_cmake \
    && cd build_cmake \
    && ../bootstrap --prefix=/usr \
    && make -j$(nproc) \
    && make install-tiledb DESTDIR="/build_thirdparty" \
    && make install-tiledb \
    && cd ../.. \
    && rm -rf tiledb 
    # && for i in /build_thirdparty/usr/lib/x86_64-linux-gnu/*; do strip -s $i 2>/dev/null || /bin/true; done \
    # && for i in /build_thirdparty/usr/bin/*; do strip -s $i 2>/dev/null || /bin/true; done

echo "------------Building OpenJPEG--------"
echo "-------------------------------------"
# Build openjpeg
# export OPENJPEG_VERSION=2.3.1
# if test "${OPENJPEG_VERSION}" != ""; then ( \
#     wget -q https://github.com/uclouvain/openjpeg/archive/v${OPENJPEG_VERSION}.tar.gz \
#     && tar xzf v${OPENJPEG_VERSION}.tar.gz \
#     && rm -f v${OPENJPEG_VERSION}.tar.gz \
#     && cd openjpeg-${OPENJPEG_VERSION} \
#     && cmake . -DBUILD_SHARED_LIBS=ON  -DBUILD_STATIC_LIBS=OFF -DCMAKE_BUILD_TYPE=Release \
#         -DCMAKE_INSTALL_PREFIX=/usr \
#     && make -j$(nproc) \
#     && make install 
    # && mkdir -p /build_thirdparty/usr/lib/x86_64-linux-gnu \
    # && rm -f /usr/lib/x86_64-linux-gnu/libopenjp2.so* \
    # && mv /usr/lib/libopenjp2.so* /usr/lib/x86_64-linux-gnu \
    # && cp -P /usr/lib/x86_64-linux-gnu/libopenjp2.so* /build_thirdparty/usr/lib/x86_64-linux-gnu \
    # && for i in /build_thirdparty/usr/lib/x86_64-linux-gnu/*; do strip -s $i 2>/dev/null || /bin/true; done \
    # && cd .. \
    # && rm -rf openjpeg-${OPENJPEG_VERSION} \
    # ); fi

# apt-get install -y --fix-missing --no-install-recommends rsync ccache

export PROJ_INSTALL_PREFIX=/usr/local/
curl -LOs  https://github.com/OSGeo/PROJ-data/releases/download/1.5.0/proj-data-1.5.zip \
&& unzip -q -j -u -o proj-data-1.5.zip  -d ${PROJ_INSTALL_PREFIX}/share/proj \
&& rm -f *.zip

# echo "------------Building Proj------------"
# echo "-------------------------------------"
# # Build PROJ
# export PROJ_VERSION=master
# mkdir -p ./proj \
#     && wget -q https://github.com/OSGeo/proj.4/archive/${PROJ_VERSION}.tar.gz -O - \
#         | tar xz -C ./proj --strip-components=1 \
#     && cd ./proj \
#     && ./autogen.sh \
#     && CFLAGS='-DPROJ_RENAME_SYMBOLS -O2' CXXFLAGS='-DPROJ_RENAME_SYMBOLS -O2' \
#         ./configure --prefix=${PROJ_INSTALL_PREFIX} --disable-static \
#     && make -j$(nproc) \
#     && make install \
#     && cd .. 
    # && make install DESTDIR="./build" \
    # && if test "${RSYNC_REMOTE}" != ""; then \
    #     ccache -s; \
    #     echo "Uploading cache..."; \
    #     rsync -ra --delete $HOME/.ccache ${RSYNC_REMOTE}/proj/; \
    #     echo "Finished"; \
    #     rm -rf $HOME/.ccache; \
    #     unset CC; \
    #     unset CXX; \
    # fi \
    # #
    # && rm -rf ./proj \
    # && PROJ_SO=$(readlink ./build${PROJ_INSTALL_PREFIX}/lib/libproj.so | sed "s/libproj\.so\.//") \
    # && PROJ_SO_FIRST=$(echo $PROJ_SO | awk 'BEGIN {FS="."} {print $1}') \
    # && mv ./build${PROJ_INSTALL_PREFIX}/lib/libproj.so.${PROJ_SO} ./build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so.${PROJ_SO} \
    # && ln -s libinternalproj.so.${PROJ_SO} ./build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so.${PROJ_SO_FIRST} \
    # && ln -s libinternalproj.so.${PROJ_SO} ./build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so \
    # && rm ./build${PROJ_INSTALL_PREFIX}/lib/libproj.*  \
    # && ln -s libinternalproj.so.${PROJ_SO} ./build${PROJ_INSTALL_PREFIX}/lib/libproj.so.${PROJ_SO_FIRST} \
    # && strip -s ./build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so.${PROJ_SO} \
    # && for i in ./build${PROJ_INSTALL_PREFIX}/bin/*; do strip -s $i 2>/dev/null || /bin/true; done

# Copy ERDAS ECW

cp -r ./erdas/ERDAS-ECW_JPEG_2000_SDK-5.4.0/Desktop_Read-Only/* /usr/local/hexagon \
&& rm -r /usr/local/hexagon/lib/x64 \
&& mv /usr/local/hexagon/lib/newabi/x64 /usr/local/hexagon/lib/x64 \
&& cp /usr/local/hexagon/lib/x64/release/libNCSEcw* /usr/local/lib \
&& ldconfig /usr/local/hexagon \
&& ldconfig /usr/local/lib

export GDAL_VERSION=release/3.0
export GDAL_RELEASE_DATE
export GDAL_BUILD_IS_RELEASE

echo "------------Building GDAL------------"
echo "-------------------------------------"
# Build GDAL
if test "${GDAL_VERSION}" = "master"; then \
        export GDAL_VERSION=$(curl -Ls https://api.github.com/repos/OSGeo/gdal/commits/HEAD -H "Accept: application/vnd.github.VERSION.sha"); \
        export GDAL_RELEASE_DATE=$(date "+%Y%m%d"); \
    fi \
    && if test "x${GDAL_BUILD_IS_RELEASE}" = "x"; then \
        export GDAL_SHA1SUM=${GDAL_VERSION}; \
    fi \
    && mkdir -p ./gdal \
    && wget -q https://github.com/OSGeo/gdal/archive/${GDAL_VERSION}.tar.gz -O - \
        | tar xz -C ./gdal --strip-components=1 \
    && cd ./gdal/gdal \
    && ./configure --prefix=/usr --without-libtool \
    --with-hide-internal-symbols \
    --with-jpeg12 \
    --with-python --with-poppler --with-mysql --with-liblzma \
    --with-webp --with-epsilon \
    --with-proj=/usr/ --with-poppler \
    --with-hdf5 --with-dods-root=/usr --with-sosi \
    --with-libtiff=internal --with-rename-internal-libtiff-symbols \
    --with-geotiff=internal --with-rename-internal-libgeotiff-symbols \
    --with-mongocxxv3 --with-tiledb \
    --with-ecw=/usr/local/hexagon \
    --with-crypto \
    && make -j$(nproc) \
    && make install DESTDIR="/build" \
    && make install \
    && cd ../.. 
    # && mkdir -p /build_gdal_python/usr/lib \
    # && mkdir -p /build_gdal_python/usr/bin \
    # && mkdir -p /build_gdal_version_changing/usr/include \
    # && mv /build/usr/lib/python3            /build_gdal_python/usr/lib \
    # && mv /build/usr/lib                    /build_gdal_version_changing/usr \
    # && mv /build/usr/include/gdal_version.h /build_gdal_version_changing/usr/include \
    # && mv /build/usr/bin/*.py               /build_gdal_python/usr/bin \
    # && mv /build/usr/bin                    /build_gdal_version_changing/usr \
    # && for i in /build_gdal_version_changing/usr/lib/*; do strip -s $i 2>/dev/null || /bin/true; done \
    # && for i in /build_gdal_python/usr/lib/python3/dist-packages/osgeo/*.so; do strip -s $i 2>/dev/null || /bin/true; done \
    # && for i in /build_gdal_version_changing/usr/bin/*; do strip -s $i 2>/dev/null || /bin/true; done
