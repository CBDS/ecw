#!/bin/bash
echo "------------Building Proj------------"
echo "-------------------------------------"
# Build PROJ
export PROJ_VERSION=8.0.0
export PROJ_INSTALL_PREFIX="/usr"
rm -rf ./proj
mkdir -p ./proj \
    && wget -q https://github.com/OSGeo/proj.4/archive/${PROJ_VERSION}.tar.gz -O - \
        | tar xz -C ./proj --strip-components=1 \
    && cd ./proj \
    && ./autogen.sh \
    && CFLAGS='-DPROJ_RENAME_SYMBOLS -O2' CXXFLAGS='-DPROJ_RENAME_SYMBOLS -O2' \
        ./configure --disable-static \
    && make -j$(nproc) \
    && make install
    # && PROJ_SO=$(readlink ./build${PROJ_INSTALL_PREFIX}/lib/libproj.so | sed "s/libproj\.so\.//") \
    # && PROJ_SO_FIRST=$(echo $PROJ_SO | awk 'BEGIN {FS="."} {print $1}')
    # && mv ./build${PROJ_INSTALL_PREFIX}/lib/libproj.so.${PROJ_SO} ./build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so.${PROJ_SO} \
    # && ln -s libinternalproj.so.${PROJ_SO} ./build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so.${PROJ_SO_FIRST} \
    # && ln -s libinternalproj.so.${PROJ_SO} ./build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so \
    # && rm ./build${PROJ_INSTALL_PREFIX}/lib/libproj.*  \
    # && ln -s libinternalproj.so.${PROJ_SO} ./build${PROJ_INSTALL_PREFIX}/lib/libproj.so.${PROJ_SO_FIRST} \
    # && strip -s ./build${PROJ_INSTALL_PREFIX}/lib/libinternalproj.so.${PROJ_SO} \
    # && for i in ./build${PROJ_INSTALL_PREFIX}/bin/*; do strip -s $i 2>/dev/null || /bin/true; done
