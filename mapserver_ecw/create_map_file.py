import argparse
import os
from jinja2 import Environment, FileSystemLoader, select_autoescape

parser = argparse.ArgumentParser(description='Creates a map file for each ecw file present in a directory tree')
parser.add_argument('-i', '--input-directory', type=str, help='Directory to search for ecw files', default='/Volumes/Elements/NL/aerial_images/full/')
parser.add_argument('-o', '--output-filename', type=str, help='Output filename to write map file to', default='./maps/all_maps.map')
parser.add_argument('-d', '--docker-directory', type=str, help='Path for maps used in docker image', default='/srv/aerial_images/')
parser.add_argument('-t', '--template-directory', type=str, help='Template filename to use for map file', default='./maps/')
args = parser.parse_args()

# Create a list of all ecw files in the tree of the input directory using glob
def get_ecw_files(input_directory, extension='.ecw'):
    return [os.path.join(r, fn)
                for r, _, fs in os.walk(input_directory, topdown=True) 
                for fn in fs if fn.endswith(extension)]     


env = Environment(loader=FileSystemLoader(searchpath=args.template_directory), autoescape=select_autoescape())
map_file_template = env.get_template("maps_template.map")
layer_file_template = env.get_template("layer_template.map")

layers = ""
ecw_files = get_ecw_files(args.input_directory)
for ecw_filename in ecw_files:
    print('Processing file: {}'.format(ecw_filename))
    # Create a map file for each ecw file
    layer_filename = os.path.basename(ecw_filename)
    layer_title = layer_filename.replace('.ecw', '')
    layer_name = layer_title.replace(' ', '').replace("_", "").upper()

    ecw_docker_path = ecw_filename.replace(args.input_directory, args.docker_directory)
    layers += layer_file_template.render(layer_name=layer_name, layer_title=layer_title, layer_projection="epsg:28992", layer_filename=ecw_docker_path)
    layers += "\n\n"

map_file_name = os.path.join(args.output_filename)
map_file = map_file_template.render(image_path=args.docker_directory, layers=layers)
with open(map_file_name, 'a') as map_file_handle:
    map_file_handle.write(map_file)
