# Mapserver

## Compiling MapServer

To compile MapServer with ECW support build the `mapserver.Dockerfile` with the following command:

```bash
docker build -f mapserver.Dockerfile . -t mapserver_ecw
```

The `mapserver.Dockerfile` compiles MapServer agains GDAL 2.4.4 with ECW support. We did not succeed in building
later versions of GDAL, because these versions need a newer version of libproj (> 6.0). This version with libproj
then conflicts with an older libproj version that is used by one of GDAL's dependencies. To build a newer version
of GDAL the source of this dependency conflict should be investigated further. Most probably, the safest route is
to build all the GDAL dependencies from source.

## Running MapServer

When the image has been built, the built image can then be ran by executing the `docker-compose` file in this directory:

```bash
docker-compose up -d
```

This `docker-compose` file starts the `mapserver_ecw` image and mounts two volumes:

- the [./maps](./maps) subdirectory is mounted /srv/data. This directory contains a main.map file that defines the services
  (wms, etc.) that are made public by MapServer. We included an example that publishes one ECW file as a WMS service.
- the directory containing the aerial images is mounted to /srv/aerial_images. This makes the ECW files available within
  the MapServer docker.

## Testing MapServer

To test the mapserver docker, try the mapserver wms, by entering the following url in your browser:

```
http://localhost/wms?service=wms&version=1.3.0&request=GetCapabilities
```

If successful this will return an xml with the map layers offered by the mapserver running. This url can also be used
with QGis to retrieve the map layers published by the wms service. In this way, the layers can be accessed for display in
QGis.

**Note**: QGis caches WMS request, to clear the cache go to Settings->Options->Network and clear the cache by clicking
the trash can button next to cache.
