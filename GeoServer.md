# Guide to build GeoServer with ECW support

This guide describes the process of how to build the OSGeo [GeoServer](http://geoserver.org/) with ECW support.
GeoServer is a server that can host several types of geospatial data, both vector and raster based data sources.
GeoServer does not support ECW files out of the box. To add ECW support, a GDAL version with ECW support needs to
be added to the GeoServer installation. This guide describes how to build a docker image containing GeoServer,
a GDAL installation with ECW support, and the GeoServer extensions for such a GDAL installation. The guide describes
the steps that are carried out in the `geoserver.Dockerfile`.

**Note:** the GeoServer we are using is the 2.18.x instead of the later 2.19.x that was also available at the time of writing. The 2.19.x version showed a strange error while trying to log in, the 2.18.x version did not.

## Creating a GeoServer docker with ECW support

To build a GeoServer docker with ECW support, we use the standard GeoServer docker file as found [here](https://hub.docker.com/r/geonode/geoserver). To this image we add a gdal installation that supports ECW (along with several other formats), and the Java extensions necessary for GeoServer to use the version of GDAL. To this cause we need to roughly carry out three steps:

1. Building GDAL with ECW support, very similar to the steps in the `Dockerfile` in this repository.
2. Building Java bindings for our version of GDAL, this process is described [here](https://trac.osgeo.org/gdal/wiki/GdalOgrInJavaBuildInstructionsUnix)
3. Installing the GeoServer GDAL extensions that are able to use our GDAL version. The installation process follows the same steps as described [here](https://docs.geoserver.org/stable/en/user/data/raster/gdal.html)

### Building GDAL with ECW support

Building GDAL with ECW support follows the same steps as described in `Dockerfile`. The main difference is that, because the GeoServer docker image uses Debian 11, the docker image that builds GDAL with ECW support also needs to be build with Debian 11. Moreover, it seems that GeoServer only support GDAL 2.x versions (as also is mentioned on the GeoServer website). GDAL 3.x versions do not seem to be detected. We therefore need to build a GDAL 2.x version. The version that worked best so far was GDAL 2.4.4. This changes some of the libraries used, which are not available anymore on Debian 11. Most notably libcharls 1.x, which now is build from source.

### Building the Java Bindings

To build the Java Bindings for GDAL, Swig and Ant need to be installed to the docker image (using apt). In addition, the geonode/geoserver2.18.x image uses a tomcat version that still uses OpenJDK 8. As Debian 11 does not provide an apt package for OpenJDK 8, we instead install the JDK manually (the image only contains the java JRE). The Java bindings are then generate using the following commands:

```bash
cd swig/java \
    && make -j$(nproc) \
  	&& make install \
```

The resulting jar and binaries then need to be copied to their respective locations in the GeoServer docker file.

`/gdal_jni/build/maven` contains gdal-3.2.3.jar, lot of warnings building java bindings and some .so files seem to be missing.

### Installing the GeoServer GDAL extensions

After the GDAL binaries are built correctly, they are copied into a standard `geonode/geoserver:2.19.x` docker image. To make the libraries available within GeoServer we need to download and install the proper GDAL extension for our GeoServer/GDAL version. This is done automatically in the docker image by carrying out the following command:

```docker
RUN mkdir -p geoserver_gdal \
	&& wget https://build.geoserver.org/geoserver/2.19.x/ext-latest/geoserver-2.19-SNAPSHOT-gdal-plugin.zip -P geoserver_gdal/ \
	&& unzip -o geoserver_gdal/geoserver-2.19-SNAPSHOT-gdal-plugin.zip  -d /usr/local/tomcat/webapps/geoserver/WEB-INF/lib
```

Here we download the zip file with the GDAL extension for our geoserver version (2.19) and unzip it into the directory that contains all geoserver extensions.

## Testing the generated docker file

To test whether the docker file can load the GDAL extensions correctly,
first start the GeoServer by using docker compose:

```bash
docker compose up -d
```

When the server has been started, open the GeoServer admin panel by going to http://localhost:8080/geoserver,
login, and open the `Stores` page. If the GDAL extensions are loaded correctly,
the Raster Data Sources section should look similar to the one in the screenshot below.

![Screenshot showing ECW plugins loaded correctly](geoserver_ecw/Images/store_formats.png)

If the ECW addon does not show up in the panel above, check the docker logs for geoserver by using:

```bash
docker compose logs | grep -i geoserver
```

If the logs show an error like the following:

```bash
it.geosolutions.imageio.gdalframework.GDALUtilities loadGDAL WARNING: Native library load failed.java.lang.UnsatisfiedLinkError: no gdaljni in java.library.path WARNING: Native library load failed.java.lang.UnsatisfiedLinkError: no gdalalljni in java.library.path*
```

This means the installation failed for one reason or another.

### Adding a WMS service for an ECW file

Adding a GDAL format, and thus also ECW files is described extensively [here](https://geoserver.geo-solutions.it/educational/en/adding_data/gdal_format.html)

To add a new ECW image go to:

Data -> Stores -> Add new Store

You should see the following screen in front of you:

![Adding a new store](geoserver_ecw/Images/store_formats.png)

Click **ECW** under **Raster Data Sources** and the following screen should appear:

![Adding an ECW data source](geoserver_ecw/Images/add_ecw_data_source.png)

Use browse to select an aerial image and add a **Data Source Name** and **Description**.
In the screenshot below, we added the 2013 CIR image and gave it a **Data Source Name** of **2013_LR_CIR**. The description we added is **CIR aerail image for 2013**.

![Adding an ECW data source (filled)](geoserver_ecw/Images/add_ecw_data_source_filled.png)

Click **Apply** to add the data source. Now to add a WMS Layer go to Data -> Layers -> Add a new layer:

![Available Layers](geoserver_ecw/Images/available_layers.png)

If the ECW store was added correctly previously, the 2013 CIR image can be selected in the combo box as can be seen below:

![Adding a New Layer](geoserver_ecw/Images/add_new_layer.png)

To publish the layer, click **Publish** and the following screen will show up.

![Publishing a New Layer](geoserver_ecw/Images/publish_layer.png)

Click **Apply** to add the layer to the list of available layers. If everything went well the layer is now available under Data -> Layer Preview:

![New Layer available in Layer Preview](geoserver_ecw/Images/layer_preview.png)

### Testing the WMS service

**Note:** Before testing the WMS service make sure to update the Proxy Base URL under Settings -> Global->Proxy Base URL. This URL defaults to http://geonode:80/geoserver but in our case will need to be most like changed to http://localhost:8080/geoserver (see below).

_OpenLayers Preview_

![Updating Proxy Base URL](geoserver_ecw/Images/update_proxy_in_global_settings.png)

To test the newly added WMS service, we can use the built in OpenLayers feature in the Layer Preview Screen.

![New Layer available in Layer Preview](geoserver_ecw/Images/layer_preview.png)

Click OpenLayers in the layer preview screen and the following screen will show up:

![OpenLayers Preview](geoserver_ecw/Images/openlayers_preview.png)

In this screen you can use the mouse and the + and - buttons to move and zoom in/out to a certain region of the Netherlands

_QGis Preview_
