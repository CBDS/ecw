# ECW

A docker file to build GDAL with the [Erdas ECW driver]
(https://download.hexagongeospatial.com/downloads/ecw/erdas-ecw-jp2-sdk-v5-4-linux)
and most common other geographical formats.

# Building the Docker file

Before building the dockerfile the Hexagon Erdas ECW driver version 5.4 needs to
be downloaded and copied to the erdas folder relative to the repository root.
The Erdas ECW driver that needs to be installed is the Linux version and can be
found here: [Hexagon Erdas ECW 5.4 Driver
Download](https://download.hexagongeospatial.com/downloads/ecw/erdas-ecw-jp2-sdk-v5-4-linux).

After downloading the Erdas Drivers and unzipping the archive, you will need to
make the ERDAS_ECWJP2_SDK-5.4.0.bin executable and run it like this:

~~~~console
chmod +x ./ERDAS_ECWJP2_SDK-5.4.0.bin
./ERDAS_ECWJP2_SDK-5.4.0.bin 
~~~~
 
To retrieve the folders with the ECW driver, first a choice has to be made
between several licence options. The docker uses the read-only driver that is
free. To continue enter 1 for "Desktop Read-Only Redistributable" and accept the
licence conditions in the next screen. When ready, a folder with the name 
hexagon will have been unpacked in the same folder that
contains the ERDAS_ECWJP2_SDK-5.4.0.bin script. In this folder, there is a
ERDAS-ECW_JPEG_2000_SDK-5.4.0 subfolder. Copy or move this folder to the
erdas folder relative to the repository root. Now the docker file can be build
with the following command:


~~~~console
 docker build . -t gdal_ecw 
~~~~

Here we build the Dockerfile and give it the tag "gdal_ecw" for future
reference. To run the docker with an interactive shell, run the following
command:

~~~~console
docker run -it gdal_ecw /bin/bash
~~~~

To check if the Docker has build GDAL correctly with ECW support run the following command 
from within the Docker (after running the previous command): 

~~~~console
gdalinfo --formats | grep -i ecw
~~~~

# Scripts to build GDAL with ECW support locally

We also have included several shell scripts in this repository to build GDAL from source with ecw support.
You can build GDAL with several dependencies and ECW support by running:

~~~~console
./build_gdal_ecw.sh
~~~~

# Using QGis with the build GDAL

QGis when installed with apt on Ubuntu has a dependency the gdal apt package. If the version
you build is the similar to the one QGis/Ubuntu ships the build version gets overwritten by that one.
Always check with gdalinfo if ecw support is still enabled:

~~~~console
gdalinfo --formats | grep -i ecw
~~~~

If not, build gdal again and it will overwrite the version installed by QGis. Sometimes the build gdal
version is still not picked up by QGis because it installed a slightly different version of the library.
In Ubuntu 20.04, for example, libgdal.so.26 is installed with QGis. ECW support in QGis can then be enabled
by replacing the symlink to that library and linking to your build version instead, for example:

~~~~console
sudo ln -s ./libgdal.so.3 libgdal.so.26
~~~~